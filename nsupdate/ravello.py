#!/usr/bin/python2
#
# a little script to queery Ravello Applications and update DNS records
#
# Copyright (C) 2019, Christian Jung
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import getopt
import getpass
import os
import tempfile
import requests

def usage():
    """ print usage information
    """
    print ("Usage:")
    print ("-u username")
    print ("-p password")
    print ("-a application name")
    print ("-k keyfile for nsupdate")
    print ("-l label")
    print ("-p prefix")
    print ("-s DNS Server (IP or FQDN)")

try:
    opts, args = getopt.getopt(sys.argv[1:], "u:p:a:k:l:")
except getopt.GetoptError as err:
    # print help information and exit:
    print ("Unknown option")  # will print something like "option -a not recognized"
    usage()
    sys.exit(2)

application = None
username = None
password = None
keyfile = None
label = None
prefix = None
server = None

for o, a in opts:
    if o == "-a":
        application = a
    elif o == "-u":
        username = a
    elif o == "-p":
        password  = a
    elif o == "-k":
        keyfile = a
    elif o == "-l":
        label = a
    elif o == "-p":
        prefix = a
    elif o == "-s":
        server = a
    else:
        print ("unhandled option")
        usage()
        sys.exit(2)

if username == None:
    username = raw_input("Enter Ravello Username: ")

if password == None:
    password = getpass.getpass("Enter Password: ")

if application == None:
    application = raw_input("Enter Ravello Application Name: ")

if keyfile == None:
    keyfile = raw_input("Enter keyfile name for nsupdate: ")

if label == None:
    label = raw_input("Enter label for DNS names: ")

if prefix == None:
    prefix = raw_input("Enter DNS Prefix for all names: ")

if server == None:
    server = raw_input("Enter DNS Server IP or FQDN: ")

if application == None or username == None or password == None or keyfile == None: 
    usage()
    sys.exit(2)

session=requests.Session()
headers={ 'Content-Type': 'application/json', 'Accept': 'application/json'}
session.post('https://cloud.ravellosystems.com/api/v1/login', auth=(username, password))

try:
    applications=session.get('https://cloud.ravellosystems.com/api/v1/applications', headers = headers)
    applist = applications.json()
except: 
    print ("Error logging into Ravello. Credentials wrong?")
    sys.exit(1)

applicationid = None
for app in applist:
    if app['name'] == application: 
        applicationid=app['id']
        break

if applicationid == None:
    print ("Could not find application with name"+application)
    sys.exit(1)

appdetails=session.get('https://cloud.ravellosystems.com/api/v1/applications/'+str(applicationid), headers = headers)
vmsinapp = appdetails.json()["design"]["vms"]

dnsrecords={}
for vm in vmsinapp:
    # try to find the opentlc hostname
    #print vm["hostnames"]
    for hostname in vm["hostnames"]:
        #print "Hostname: "+hostname
        actualhostname = None
        if "example" in hostname:
            actualhostname=hostname.split(".")[0]
            break

    if actualhostname == None:
        print ("Could not find an example.com hostname for the VM "+str(vm["hostnames"])+", check application and blueprint")
        sys.exit(2)

    for nic in vm["networkConnections"]:
        #print "Nic:"+str(nic)
        if "ipConfig" in nic:
            # make sure the nic has a public IP, aka. is accessible from Internet
            if nic["ipConfig"]["hasPublicIp"]==False:
                break
            if "fqdn" in nic["ipConfig"]:
                dnsrecords[actualhostname]=nic["ipConfig"]["fqdn"]

tempfilename = "/tmp/nsupdate"
print ("server"+server)
print ("prefix"+prefix)
tempfile = open(tempfilename,'w')
tempfile.write("server "+server+"\nzone "+prefix+".")
for dnsrecord in dnsrecords.keys():
    # apparently you can not update all those records in one bunch
    # have to call nsupdate individually for each record

    shortname = dnsrecord.split('.')[0]
    print ("Creating CNAME Records for "+shortname+"-"+label+"."+prefix+" (https://"+shortname+"-"+label+"."+prefix+")")
    tempfile.write("\nupdate delete "+shortname+"-"+label+"."+prefix+".")
    tempfile.write("\nupdate add "+shortname+"-"+label+"."+prefix+". 86400 CNAME "+dnsrecords[dnsrecord]+".\n")

tempfile.write("\nsend\n")
tempfile.flush()
os.fsync(tempfile)
tempfile.close
    
rc=os.system("nsupdate -k "+keyfile+" "+tempfilename)
if rc != 0:
    print ("Something didn't work: Return Code =",rc)
    sys.exit(1)
