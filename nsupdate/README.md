# README

This script can create dynamic DNS records for Ravello instances. It's looking for the short hostname and creates DNS records for it.

This is helpful because the public FQDN's in Ravello are randomized and therefore not predictable. This script creates CNAME records following a predictable scheme.
