# README

This repository contains a collection of small scripts and tools which are too small for a dedicated repository.

## nsupdate

A helper script to create predictable CNAME's for Ravello Applications.
